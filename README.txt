programmé sous Windows
programme Python 3.11 module tkinter et math.

Pour lancer ce programme il vous suffit d'ouvrir le fichier "main" dans IDLE et de faire "run". 
Pour pouvoir jouer au jeu, il suffit de cliquer avec la souris sur la pièce que l'on veut déplacer et ensuite de cliquer sur la case où l'on veut quelle soit 
Les règles du jeu et les déplacements sont dans le répertoire "Doc" puis le fichier "règles des échecs".
Une fois que vous connaissez ces règles vous pourrez faire une partie.

dossier img : le dossier avec les images 
fichier draw.py : fichier avec le programme principal
fichier main.py : fichier de lancement
fichier var.py : fichier avec les variables